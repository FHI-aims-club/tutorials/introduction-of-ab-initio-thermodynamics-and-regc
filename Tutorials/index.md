# Tutorial: Introduction of ab initio Thermodynamics and REGC

This tutorial introduces the main concepts of *ab initio* atomistic thermodynamics and the replica-exchange grand-canonical (REGC) scheme using FHI-aims.

A temperature-pressure phase diagram describes the composition and structure of a system at thermal equilibrium and is an essential tool for understanding material properties. The well established method of *ab initio* atomistic thermodynamics has been highly successful in predicting surface phase diagrams in realistic $T,p$ conditions [1, 2, 3]. While catalysis and growth are not processes that are occurring in thermal equilibrium, it is invaluable to find phases that may exist at temperature and pressure conditions of optimum catalyst performance or during growth for any meaningful analysis. Results obtained using ab initio atomistic thermodynamics, however, rely on two approximations. One is that the phase space only consists of a fixed and predetermined number of structures (handpicked by us). The other is that vibrational contributions from the adsorbates and any anharmonic effects are commonly neglected. These approximations do not always yield accurate phase diagrams, especially at high temperature and/or coverage. An unbiased sampling of the configurational and compositional space may, in contrast, reveal additional metastable structures.

To this end, we will also introduce the recently developed REGC sampling method in combination with molecular dynamics [4]. The resulting phase diagrams include all vibrational contributions, including anharmonic effects, and thus will more accurately model realistic growth $T,p$ conditions, albeit at (much) higher computational cost.

This tutorial was a joint work by (in alphabetical order) Volker Blum, Sebastian Kokott, Konstantin Lion, and Yuanyuan Zhou.

## Objectives of This Tutorial

This tutorial introduces both methods to simulate surface/cluster stability. The tutorial is structured as:

* Creating slab structures for FHI-aims and how to perform Slab calculations in FHI-aims. Please go to the tutorial on [Slab Calculations and Surface Simulations with FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/slab-calculations-and-surface-simulations-with-fhi-aims).
* Surface stability with ab initio atomistic thermodynamics
* Calculating phase diagrams using REGC

## Prerequisites

* A sufficient understanding about the basics of running FHI-aims is required. Please review our tutorial [Basics of Running FHI-aims tutorial](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/) if you have not yet done so and/or if you are unfamiliar with the code.

* A sufficiently powerful computer. For this tutorial a laptop with at least two (physical cores) should be sufficient.

* The FHI-aims code distribution must be present on this computer.

* A python installation with the [ASE](https://wiki.fysik.dtu.dk/ase/index.html) and/or [pymatgen](https://pymatgen.org) libraries. These libraries are needed for the creation of the surface structures. 

The complete set of input and output files of this tutorial are available at <https://gitlab.com/FHI-aims-club/tutorials/introduction-of-ab-initio-thermodynamics-and-regc>, which can be easily obtained by cloning this repository onto your own computer.

## Useful links

The following links contain useful information and tools regarding this tutorial and FHI-aims in general:

- The present gitlab repository, which contains all the documents and simulation data for this tutorial: <https://gitlab.com/FHI-aims-club/tutorials/introduction-of-ab-initio-thermodynamics-and-regc> 
- The CLIMS gitlab repository, which contains useful utilities that can be used alongside with FHI-aims to facilitate input files preparation and/or output data analysis, in particular [CLIMS](https://gitlab.com/FHI-aims-club/utilities/clims): <https://gitlab.com/FHI-aims-club/utilities>
- FHI-aims browser-based Graphical Interface for Materials Simulation (GIMS): <https://gims.ms1p.org>


** References **

[1] [K. Reuter and M. Scheffler, "Composition and structure of the RuO2(110) surface in an O2 and CO environment: Implications for the catalytic formation of CO2", Phys. Rev. B, 68, 045407, (2003).](https://doi.org/10.1103/PhysRevB.68.045407)

[2] [K. Reuter and M. Scheffler, "First-Principles Atomistic Thermodynamics for Oxidation Catalysis: Surface Phase Diagrams and Catalytically Interesting Regions", Phys. Rev. Lett., 90, 046103, (2003).](https://doi.org/10.1103/PhysRevLett.90.046103)

[3] [Schewski, et al. "Step-ﬂow growth in homoepitaxy of $\beta$-Ga$_2$O$_3$ (100)—The inﬂuence of the miscut direction and faceting." APL Mater. 7, 022515 (2019).](https://doi.org/10.1063/1.5054943)

[4] [Y. Zhou, M. Scheffler, and L. M. Ghiringhelli, "Determining surface phase diagrams including anharmonic effects", Phys. Rev. B 100, 174106 (2019).](https://doi.org/10.1103/PhysRevB.100.174106)