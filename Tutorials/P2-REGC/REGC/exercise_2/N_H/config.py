# input the required data
import os

n_metal = 2 
number_replica = 32 
EE=-15.8701367945
Emetal=-7895.88305479083
############### Constant ###############################
h      = 4.135667662e-15 # Plank constant unit is eV*s
kB     = 8.6173325e-5    # Boltzmann constant in eV/K
pi     = 3.14159265359   # the value of pi
c      = 2.99792458e18   # Speed of light in Angstrom/s
############## End of Constant ##########################
