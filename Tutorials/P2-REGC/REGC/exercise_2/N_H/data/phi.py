import numpy
helpfile=open('replica-indices','r')
helines=helpfile.readlines()
infile=open('num_particles','r')
lines = infile.readlines()
line_num = len(lines)
num_replica = 20
N_kt = numpy.zeros([num_replica,line_num], numpy.float32)
for i in range(line_num):
        line = lines[i]
        heline = helines[i]
        elements = line.split()
        heelements = heline.split()
        for j in range(num_replica):
                N_kt[int(heelements[j]),i] = elements[int(heelements[j])]
for i in range(num_replica):
        outfile=open(str(i)+'.phi','a+')
        for j in range(line_num):
                outfile.write(str(N_kt[i,j])+'\n')
        outfile.close()
