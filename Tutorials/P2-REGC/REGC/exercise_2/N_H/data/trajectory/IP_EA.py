import os
import numpy as np
for i in range(20):
	infile1 = open(str(i)+'.phi', 'r')
	infile2 = open(str(i)+'.psi', 'r')
	lines1  = infile1.readlines()
	lines2  = infile2.readlines()
	E_IP    = []
	E_EA    = []
	for j in range(len(lines1)):
		E_IP.append(float(lines1[j].split()[0]))
		E_EA.append(float(lines2[j].split()[0]))
	E_IP    = np.array(E_IP)
	E_EA    = np.array(E_EA)
	E_gap   = E_IP - E_EA
	oufile  = open(str(i)+'gap', 'a+')
	for k in range(len(lines2)):
		oufile.write(str(E_gap[k])+'\n')
