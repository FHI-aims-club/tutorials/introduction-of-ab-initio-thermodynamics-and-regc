#!/bin/sh
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=4
##SBATCH --gres=gpu:2
#SBATCH --time=05:00:00
#SBATCH --partition=p.talos
#SBATCH --job-name=NAME
#SBATCH --output=NAME-%j.out
#SBATCH --error=NAME-%j.error

ulimit -s unlimited;

module purge
module load intel/19.1.3 mkl/2019.3 impi/2019.9 cuda/10.1
module list


#binary=/u/yzhou/software/FHIaims/build/aims.190519.scalapack.mpi.x
#binary=/u/yzhou/software/FHIaims_plumed/build/aims.190519.scalapack.mpi.x
binary=/u/yzhou/software/2020/FHIaims-plumed/build/aims.200212.plumed.scalapack.mpi.x
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1


# Run the program:
python core.py >log.core
