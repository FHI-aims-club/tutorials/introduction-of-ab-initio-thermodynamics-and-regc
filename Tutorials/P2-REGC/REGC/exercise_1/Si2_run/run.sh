#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --partition=big
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=4

source /software/Intel/oneapi.2023.1.0/setvars.sh

source ~/software/miniconda3/bin/activate

source ~/.bashrc
ulimit -s unlimited


#srun /home/htc/yuzhou/software/FHIaims/build/aims.240910.meta.scalapack.mpi.x >aims.out
#mpirun /home/htc/yuzhou/software/VASP/vasp.5.4.4/bin/vasp_std
python core.py>logcore
