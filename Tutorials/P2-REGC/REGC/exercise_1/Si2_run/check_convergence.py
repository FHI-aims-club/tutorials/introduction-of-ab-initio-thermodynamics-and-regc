#!/usr/bin/python -u
# Copyright (C) Yuanyuan Zhou
# Author : Yuanyuan Zhou < zhou@fhi-berlin.mpg.de >
#
# This file is part of REGCMC PACKAGE.
# Version 2014.06.08
import numpy as np
import os
def getEN(mu):
    """sort Struct into different replicas"""
    os.system('grep Energy replica_0'+str(mu)+'/replica_* >E_' + str(mu) )
    os.system('grep Ele_Num replica_0'+str(mu)+'/replica_* >N_' + str(mu) )

for i in range(4):
        getEN(i)
