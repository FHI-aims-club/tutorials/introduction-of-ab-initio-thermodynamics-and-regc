import copy
from config import *
import random
from Aims_E import calculateE_Aims
import math
import numpy
from hardspherical import *

def mcmove(coord, T, e, nacc_d):
# randomly select one of n atoms and randomly choose the x/y/z coordinate of specific atoms
	beta        = 1.0/(T*kB)
	coord_new_d = copy.deepcopy(coord)
	if len(coord_new_d) > n_metal:
		e_former    = e
	       	j           = random.randrange(n_metal,len(coord_new_d))
# Perform trial move - random translation in 3d
       	        coord_new_d[j][0]=coord_new_d[j][0]+ step * ( random.random() - 0.5 )
               	coord_new_d[j][1]=coord_new_d[j][1]+ step * ( random.random() - 0.5 )
                coord_new_d[j][2]=coord_new_d[j][2]+ step * ( random.random() - 0.5 )
# First check boundary condition 1=pass,0=fail
		boundary = HardSphericalWall(coord_new_d,center,latt)
# Then check the distance between Au-Au,Au-O,O-O
       	        btag = boundary.CheckIFInside(j)
		otag = boundary.overlapd(j)
             	if btag==1 and otag==1:
        	        e_new = calculateE_Aims(coord_new_d) 
       	        	A_mov = math.exp( -beta *(e_new - e_former))
        	        if A_mov > random.random():
	        		coord = coord_new_d
        	        	e = e_new
				nacc_d = nacc_d + 1
                                if (coord[j][0] < 0):
	                                coord[j][0] = coord[j][0] + latt[0]
                                if (coord[j][0] > latt[0]):
        	                        coord[j][0] = coord[j][0] - latt[0]
                                if (coord[j][1] < 0):
                                        coord[j][1] = coord[j][1] + latt[1]
                                if (coord[j][1] > latt[1]):
                                        coord[j][1] = coord[j][1] - latt[1]
		else:
			pass 
	return coord, e, nacc_d
