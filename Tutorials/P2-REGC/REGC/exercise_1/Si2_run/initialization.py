import sys
import os
import config
import numpy as np
def initialization():
	coord_fly = config.coord_list
	lattice   = np.zeros((3,3))
	for i in range(3):
		for j in range(3):
			if i==j:
				lattice[i][j] = config.box
			else:
				lattice[i][j] = 0.0
	for rank in range(config.num_replica):
		structure = coord_fly[rank]
		srank='%.2d' %rank
		geometry_file = open(os.path.join('replica_'+srank, 'geometry.in'), 'w')
		for i_l in range(3):
			geometry_file.write(' '*4+'%8s %15.5f %15.5f %15.5f\n' %('lattice_vector', lattice[ii][0], lattice[ii][1], lattice[ii][2]))
		for ii in range(len(structure)):
			if ii<config.n_metal:
				geometry_file.write(' '*4+'%8s %15.5f %15.5f %15.5f %8s\n' %('atom', structure[ii][0], structure[ii][1], structure[ii][2]), config.nameofatoms[0])
			else:
				geometry_file.write(' '*4+'%8s %15.5f %15.5f %15.5f %8s\n' %('atom', structure[ii][0], structure[ii][1], structure[ii][2]), config.nameofatoms[1])
       		geometry_file.close()
