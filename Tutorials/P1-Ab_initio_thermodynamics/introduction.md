# Introduction

As mentioned earlier, *ab initio* atomistic thermodynamics is a well established method to describe surface stability in $T,p$ conditions. If the surface system is in contact with a thermodynamical reservoir at constant $T$, $V$, and $p$, the system will minimize the Gibbs free energy $G$. 

The surface free energy $\gamma$ is a measure of surface stability and can be written as [1]:
$$
\gamma(T, \{p_i \}) = \frac{1}{A} \left( G^{sur}(T, \{p_i \}) - \sum_i N_i \, \mu_i(T, \{p_i \}) \right) \, .
$$
Here, $A$ is the surface area, $G^{sur}$ the Gibbs free energy, $N_i$ the number of atoms and $\mu_i$ the chemical potential for species $i$. The surface free energy is evaluated for a temperature $T$ and the partial pressures of all species. The lower the surface free energy, the higher the stability of the surface.

Within FHI-aims we simulate surfaces via slabs. By definition a slab has two surfaces. In case of equivalent top and bottom surfaces, we can express the surface energy as:
$$
\gamma(T, \{p_i \}) = \frac{1}{2A} \left( G^{slab}(T, \{p_i \}) - \sum_i N_i \, \mu_i(T, \{p_i \}) \right) \, .
$$
Now $A$ refers to the area of the surface unit cell and the factor $2$ in the denominator corresponds to the two equal surface sides.

## The Gibbs free energy of the solid bulk/surface

A key quantity in calculating the surface free energy is the Gibbs free energy $G$. It can be separated into several terms
$$
G(T,P) = E_{total} + F_{vib} + F_{conf} + pV \, ,
$$
where $E_{total}$ is the total (internal) energy, $F_{vib}$ the vibrational free energy, and $F_{conf}$ the configurational free energy. In most instances we are interested in relative surface stabilities, i.e. *differences in surface energy*. This can allow for quite some degree of error cancellation, especially if we compare the Gibbs free energies of solid bulk and solid surface. 

The dominant term is the total energy $E_{total}$ which is calculated by DFT calculations. It is common to neglect all other terms when discussing relative surface stabilities. This, however, has to be carefully checked for each use case. A more detailed discussion of all terms can be found in [1, 2]. We will also investigate the effect of the vibrational contributions in one of the upcoming examples.

## Stoichometric surfaces

We only want to consider stoichometric surfaces in this tutorial. Stoichometric surfaces contain the same ratio of species atoms as the bulk material. The surface free energy in this case reduces to a much simpler formula:
$$
\gamma(T,p) = \frac{1}{2A} \left( G^{slab}(T,p) - N\cdot g_{bulk}(T,p) \right) \, .
$$
Here, $g_{bulk}$ refers to Gibbs free energy of the bulk cell per formula unit and N is the number of repeated formula units in the slab. We will also approximate the Gibbs free energy as
$$
G = E^{DFT} \, ,
$$
following the discussion in the introduction. We therefore calculate the surface free energies as:
$$
\gamma = \frac{1}{2A} \left( E^{slab} - N\cdot e_{bulk} \right) \, .
$$
Notice how the surface free energies are now *independent* of temperature and pressure in the case of stochoimetric surfaces in this approximation.

This is typically a good starting point for any analysis of relative surface stabilites and may already explain experimental findings. We demonstrate this for the example of $\beta$-Ga$_2$O$_3$ surfaces [3].

**References**

[1] [J. Rogal and K. Reuter, "Ab Initio Atomistic Thermodynamics for Surfaces: A Primer." Educational Notes RTO-EN-AVT-142, 2007, pp. 2–1–2–18.](https://th.fhi-berlin.mpg.de/th/publications/EN-AVT-142-02.pdf)

[2] [C. Sutton and S. V. Levchenko, "First-Principles Atomistic Thermodynamics and Configurational Entropy", Front. Chem., 03 (2020).](https://doi.org/10.3389/fchem.2020.00757)

[3]: [Schewski, et al. "Step-ﬂow growth in homoepitaxy of $\beta$-Ga$_2$O$_3$ (100)—The inﬂuence of the miscut direction and faceting." APL Mater. 7, 022515 (2019).](https://doi.org/10.1063/1.5054943)