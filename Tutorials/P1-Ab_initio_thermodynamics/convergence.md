# Convergence of the surface energy

In the previous section, you have learned the basic concepts of calculating surface energies. Now we test its convergence behaviour for the Si(100) example.

## Convergence with number of bilayers

To obtain converged surface energies, we need a large enough vacuum between slabs until there is no more interaction between adjacent periodic images, and increase the slab thickness until there is no interaction between opposite surfaces through the bulk. A well-documented phenomenon is that for some surfaces the calculated surface energy does not converge with respect to increasing slab size [1]. We demonstrate this behaviour for the Si(100) surface.

We have calculated the surface energies for slabs ranging from 4 to 20 bilayers. The results are available in the `bilayer_convergence` folder in the `solutions` folder. It is known that the surface energy of this example calculated from the slab total energy and the conventional bulk reference energy (the same holds true for the primitive bulk cell) diverges with an increasing amount of bilayers. The main origin of this divergence can be attributed to a non-rigorous definition of the bulk reference energy [2,3], i.e., the energies of the slab and bulk reference unit cell are not calculated in a way that is exactly consistent (e.g. with respect to the reciprocal k-mesh).

![Si100_divergence](img/Si_100_convergence_conventional.png)

As described in [2], we can instead obtain a bulk reference energy by a linear fit of the slab energies versus the number of bilayers:
$$
E_{slab}(N) \approx 2\,A\,\gamma + N\cdot e_{bulk} \, .
$$
The slope is then the new bulk reference energy. This results in much better converged surface energies, since we do not use a separately calculated bulk reference. The resulting plot looks like:

![Si100_convergence](img/Si_100_convergence.png)

Using this linear fit method, we can get a converged surface energy of $\gamma\approx1.487$ eV/Å$^2$. The clear downside of this method is the fact that we need several slab calculations to obtain a reliable linear fit model. Other possible ways to obtain converged surface energies are described in [2] and [3].

This convergence issue can also be overcome by using an extremely well-converged k-grid for the slab. This strategy, however, might not be feasible for larger slabs. 

## Convergence with basis set size

The surface energies are not heavily dependent on the employed species defaults. We can see this for the typical `light`, `intermediate`, and `tight` settings in this plot:

![Si100_convergence_basis](img/Si_100_convergence_basis.png)

This is the result of calculating the surface energies as energy differences, i.e. errors from using smaller basis sets are cancelled out to a large degree. The shown surface energies were calculated for the slab used in the workflow section.

** References **

[1] [J. C. Boettger, "Nonconvergence of surface energies obtained from thin-film calculations", Phys. Rev. B 49, 16798 (1994).](https://doi.org/10.1103/physrevb.49.16798)

[2] [V. Fiorentini and M Methfessel, "Extracting convergent surface energies from slab calculations", J. Phys.: Condens. Matter 8, 6525 (1996).](https://doi.org/10.1088/0953-8984/8/36/005)

[3] [Sun, W., Ceder, G., "Efficient creation and convergence of surface slabs", Surface Science, 617, 53–59 (2013).](https://doi.org/10.1016/j.susc.2013.05.016)
