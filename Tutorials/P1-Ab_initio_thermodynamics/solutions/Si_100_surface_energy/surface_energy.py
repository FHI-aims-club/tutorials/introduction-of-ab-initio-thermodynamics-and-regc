# Script to calculate the surface energy of the Si(100) surface

A = 14.74245
e_bulk = -62993.167948580 / 8.
E_slab = -62988.845773205

gamma = 1/(2*A) * (E_slab - 8.*e_bulk)

E_slab_reconstructed = -125981.03186663

gamma_reconstructed = 1/(4*A) * (E_slab_reconstructed - 16.*e_bulk)
print("ideal gamma = {0} eV/Angs^2".format(gamma))
print("reconstructed gamma = {0} eV/Angs^2".format(gamma_reconstructed))
