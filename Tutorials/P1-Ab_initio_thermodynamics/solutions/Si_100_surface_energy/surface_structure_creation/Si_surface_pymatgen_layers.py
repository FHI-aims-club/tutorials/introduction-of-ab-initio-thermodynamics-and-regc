from pymatgen.core.surface import SlabGenerator, Lattice, Structure
from pymatgen.io.ase import AseAtomsAdaptor

# Pymatgen doesn't support the aims input/output format yet
# We use ase to do the geometry output
ase_py = AseAtomsAdaptor()

# Create the diamond Si bulk first
lattice = Lattice.cubic(5.43)
Si = Structure(lattice, ["Si", "Si", "Si", "Si",
                         "Si", "Si", "Si", "Si"],
               [[0.00000, 0.00000, 0.50000],
                [0.75000, 0.75000, 0.75000],
                [0.00000, 0.50000, 0.00000],
                [0.75000, 0.25000, 0.25000],
                [0.50000, 0.00000, 0.00000],
                [0.25000, 0.75000, 0.25000],
                [0.50000, 0.50000, 0.50000],
                [0.25000, 0.25000, 0.75000]])


# Now we use the pymatgen SlabGenerator to create the Si(100) surface
no_bulk_layers = [6]
for layers in no_bulk_layers:
    slabgen = SlabGenerator(Si, (1, 0, 0), min_slab_size=layers, min_vacuum_size=6, in_unit_planes=True, center_slab=True)
    Si_100 = slabgen.get_slabs()[0]

    ase_struc = ase_py.get_atoms(Si_100)

    ase_struc.write("Si_100_" + str(layers * 2) + "_bilayers.in", scaled=True)
