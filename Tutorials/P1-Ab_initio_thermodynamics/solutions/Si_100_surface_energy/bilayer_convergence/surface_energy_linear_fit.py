# Script to calculate the surface energy of the Si(100) surface

from matplotlib import pyplot as plt
import numpy as np


def get_no_atoms(outfile):
    with open(outfile, "r") as fd:
        line = next(l for l in fd if "| Number of atoms" in l)
        no_at = float(line.split()[-1])

    return no_at


def extract_total_energy(outfile):
    with open(outfile, "r") as fd:
        line = next(l for l in fd if "| Total energy of the DFT / Hartree-Fock s.c.f. calculation" in l)
        e = float(line.split()[-2])

    return e


def surface_energy_stoicho(slab_data, E_bulk, A):
    """ Calculate the surface energy for a stoichometric surface. """
    E_slab = extract_total_energy(slab_data)
    N_slab = get_no_atoms(slab_data)

    gamma = 1 / (2. * A) * (E_slab - N_slab * E_bulk)

    return gamma


A = 14.74245

bulk_data = "./bulk/aims.out"
bulk_E = extract_total_energy(bulk_data)

bilayers = [4, 6, 8, 10, 12, 16, 20]
layers_data = [str(i) + "_bilayer/aims.out" for i in bilayers]

gamma = []
E_slab = []
N_slab = []

for layer in layers_data:
    E_slab.append(extract_total_energy(layer))
    N_slab.append(get_no_atoms(layer))

# Do a linear fit to obtain the bulk energy
z = np.polyfit(N_slab[2:], E_slab[2:], 1)

p1 = np.poly1d(z)

E_bulk = p1[1]

for layer in layers_data:
    gamma.append(surface_energy_stoicho(layer, E_bulk, A))

print(gamma)

# Plot the data
#colors = ["tab:blue", "tab:orange", "tab:green", "tab:purple"]

plt.plot(bilayers, gamma, "-x")
# plt.plot(bilayers[1:], fit)

plt.ylim(0.99 * min(gamma), max(gamma) * 1.01)
plt.xlabel("Number of bilayers")
plt.ylabel(r"Surface energy (eV/$\AA^2$)")
plt.legend()
plt.savefig("Si_100_convergence.png")
