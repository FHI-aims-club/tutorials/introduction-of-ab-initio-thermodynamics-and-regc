# Script to calculate the surface energy of the Si(100) surface

from matplotlib import pyplot as plt


def get_no_atoms(outfile):
    with open(outfile, "r") as fd:
        line = next(l for l in fd if "| Number of atoms" in l)
        no_at = float(line.split()[-1])

    return no_at


def extract_total_energy(outfile):
    with open(outfile, "r") as fd:
        line = next(l for l in fd if "| Total energy of the DFT / Hartree-Fock s.c.f. calculation" in l)
        e = float(line.split()[-2])

    return e


def surface_energy_stoicho(slab_data, bulk_data, A):
    """ Calculate the surface energy for a stoichometric surface. """
    E_slab = extract_total_energy(slab_data)
    E_bulk = extract_total_energy(bulk_data)
    N_slab = get_no_atoms(slab_data)

    gamma = 1 / (2. * A) * (E_slab - N_slab * (E_bulk / 8.))

    return gamma


A = 14.74245

bulk_data = "./bulk/aims.out"
bilayers = [4, 6, 8, 10, 12, 16, 20]
layers_data = [str(i) + "_bilayer/aims.out" for i in bilayers]
gamma = []

for layer in layers_data:
    gamma.append(surface_energy_stoicho(layer, bulk_data, A))

# Plot the data
#colors = ["tab:blue", "tab:orange", "tab:green", "tab:purple"]

plt.plot(bilayers, gamma, "-x")

plt.xlabel("Number of bilayers")
plt.ylabel(r"Surface energy (eV/$\AA^2$)")
plt.legend()
plt.savefig("Si_100_convergence_conventional.png")
