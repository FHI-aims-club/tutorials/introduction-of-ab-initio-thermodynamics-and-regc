from pathlib import Path
import subprocess

p = Path(".")

command = "mpirun -np 4 /users/stud/lion/FHI-aims/Tutorial_series/fhi-aims.210716_2/fhi-aims.210716_2/build/aims.210716_2.scalapack.mpi.x > aims.out"

folder = ["8_bilayer","10_bilayer"]

for calc in folder:
    fold = p/calc
    print(fold)
    proc = subprocess.Popen(command, shell=True, cwd=fold)
    errorcode = proc.wait()
    print(f"-- Calculation Finished")

