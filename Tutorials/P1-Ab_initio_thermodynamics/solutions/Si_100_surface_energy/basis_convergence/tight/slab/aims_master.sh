#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./Output
#SBATCH -e ./Error
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J Jobscript
# Queue (Partition):
#SBATCH --partition=tiny
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=40
#
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=lion@fhi-berlin.mpg.de
#
# Wall clock limit:
#SBATCH --time=01:50:00

module purge
module load intel/19.1.1 impi/2019.7 mkl/2020.1
module load anaconda
ulimit -s unlimited
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1
export MKL_DYNAMIC=FALSE
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MKL_HOME/lib/intel64
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mpcdf/soft/SLE_12/packages/x86_64/intel_parallel_studio/2020.1/lib/intel64/
export TMPDIR=/ptmp/kmlion/tmp/$SLURM_JOBID

#export aimsbin=aims.200112.scalapack.mpi.x
export aimsbin=~/fhi-aims_git/new_master/FHIaims/build/aims.211010.meta.scalapack.mpi.x

date
srun $aimsbin > aims.out
date
