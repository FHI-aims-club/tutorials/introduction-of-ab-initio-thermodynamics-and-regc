# Surface stability of Ga2O3

!!! Warning
    The surface slabs are very big for this system ($>$60 atoms). The data for the surface energies and vibrational contributions are available in the [git repository](https://gitlab.com/FHI-aims-club/tutorials/introduction-of-ab-initio-thermodynamics-and-regc/-/tree/main/Tutorials/P2-Ab_initio_thermodynamics/solutions/Ga2O3).

We want to apply the concepts of ab initio atomistic thermodynamics to a specific example. Our system of choice is $\beta$-Ga$_2$O$_3$, a transparent conducting oxide. The following results were published in [1]. 

Depending on the miscut direction on off-oriented (100) $\beta$-Ga$_2$O$_3$ substrates, homoepitaxially grown layers show distinct surface morphologies, i.e., the formation of (2̅01) facets [1]. The following results support the experimental findings that (2̅01) faceting on (100) substrates is indeed thermodynamically favored at the conditions during homoepitaxial growth. 

The conventional bulk cell consists of octahedrally (dark green) and tetrahedrally (light green) coordinated gallium atoms and 3 inequivalent oxygen atoms (orange, yellow, red) in a monoclinic unit cell. As a result, many possible surface directions have to be considered. The picture below was constructed with [VESTA](https://jp-minerals.org/vesta/en/).

![vesta_Ga2O3_bulk](img/Ga2O3_bulk.png)

The surface free energy for this system can be written as:
$$
\gamma(T, p_O, p_{Ga}) = \frac{1}{2A} \left( G^{slab}(T, p_O, p_{Ga}) - N_{Ga} \, \mu_{Ga}(T, p_{Ga}) - N_{O} \, \mu_{O}(T, p_{O}) \right) \, .
$$
We only want to consider stoichometric surfaces in this example. Stoichometric surfaces contain the same ratio of Ga to O atoms, e.g. 2:3, as the bulk material. The surface free energy in this case reduces to a much simpler formula (see also the introduction to P2):
$$
\gamma = \frac{1}{2A} \left( E^{slab} - N\cdot e_{Ga_2O_3} \right) \, .
$$

We will consider a total of 9 low-index surface terminations. All surface structures were constructed using the pymatgen library, as introduced in section P0. 

## Dependence on XC functional

The surface free energies of Ga$_2$O$_3$ are not strongly dependent on the employed XC functional, as can be seen in the following plot.

![SE_Ga2O3](img/surface_energy_Ga2O3.png)

This result is not very surprising, since surface energies are calculated as *energy differences*. The inherent errors in semi-local functionals are thus cancelled out to a large degree.

## Effect of vibrational contributions

We can recover the temperature dependence of the surface free energy by including the vibrational free energy $F_{vib}$ explicitly. We calculate this term using [phonopy](https://phonopy.github.io/phonopy/) in the harmonic approximation for the bulk and all surface structures. (A dedicated tutorial for phonon calculations in FHI-aims will also be available.) We can then separate the surface free energy in two parts:
$$
\gamma_{tot}(T) = \gamma + \gamma_{vib}(T) \, .
$$

The vibrational part can be expressed as

$$
\gamma_{vib}(T) = \frac{1}{2A} \left( F^{slab}_{vib}(T) - N\cdot F^{bulk}_{vib}(T) \right) \, .
$$

The vibrational contribution $\gamma_{vib}(T)$ for all surfaces is shown in the following plot:

![Ga2O3_vib](img/vibrational_free_energy_Ga2O3.pdf)

We can see that for typical growth temperatures of $T\approx 1000$K the vibrational part reduces the surface free energy further. The relative decrease in all cases is around 10%. More importantly, the relative stability of the surfaces is not affected. This can be seen in the following plot:

![Ga2O3_SE_vib](img/surface_energy_Ga2O3_1000K_stoichometric.pdf)

Note that we do not consider anharmonic vibrational contributions that may affect the results in some cases. 

**References**

[1]: [Schewski, et al. "Step-ﬂow growth in homoepitaxy of $\beta$-Ga$_2$O$_3$ (100)—The inﬂuence of the miscut direction and faceting." APL Mater. 7, 022515 (2019).](https://doi.org/10.1063/1.5054943)
